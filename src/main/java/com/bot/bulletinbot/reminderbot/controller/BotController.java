package com.bot.bulletinbot.reminderbot.controller;


import com.bot.bulletinbot.reminderbot.service.ReminderBotService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

@RestController
@LineMessageHandler
public class BotController {
    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private ReminderBotService reminderBotService;

    private long counter=0;

    @EventMapping
    @Timed("bot-request")
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) throws ParseException {
        String userId = messageEvent.getSource().getUserId();
        String message = messageEvent.getMessage().getText().toLowerCase();
        String[] splitInput = message.split(" ");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        // Default Response
        String answer =" Halo! ketik 'help' untuk melihat daftar command yang ada";

        if (message.contains("/reminder help")) {
            answer = "'/reminder create [date(dd-MM-yyyy hh:mm) subject]’, \n " +
                    "'/reminder all',\n" +
                    "'/reminder id [id]',\n" +
                    "'/reminder update [id] [date(dd-MM-yyyy hh:mm) subject',\n" +
                    "'/reminder delete [id]'";

        } else if (message.contains("/reminder create ")) {
            String strDate = splitInput[2]+" "+splitInput[3];
            Date date = formatter.parse(strDate);
            String rawMessage = message.replaceAll("/reminder create"+" "+strDate+" ","");

            answer = reminderBotService.createReminder(++counter,userId,date, rawMessage);

        } else if (message.contains("/reminder all")) {
            answer = reminderBotService.getReminderByUser(userId);

        } else if (message.contains("/reminder id ")) {
            answer = reminderBotService.getReminderByID(Long.parseLong(splitInput[2]),userId);

        } else if (message.contains("/reminder update ")) {
            String strDate = splitInput[3]+" "+splitInput[4];
            Date date = formatter.parse(strDate);
            String rawMessage = message.replaceAll("/reminder update"+" "+splitInput[2]+" "+strDate+" ","");
            answer = reminderBotService.updateReminder(Long.parseLong(splitInput[2]),userId,date, rawMessage);

        }else if (message.contains("/reminder delete ")) {
            answer = reminderBotService.deleteReminderById(Long.parseLong(splitInput[2]),userId);

        }
        String replyToken = messageEvent.getReplyToken();
        handleReplyEvent(replyToken, answer);

    }

    /**
     * Handle reply message.
     *
     * @param replyToken Token as User identifier.
     * @param answer     Message that will be send to User.
     */
    public void handleReplyEvent(String replyToken, String answer) {
        TextMessage answerInTextMessage = new TextMessage(answer);
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, answerInTextMessage))
                    .get();
        } catch (NullPointerException | InterruptedException | ExecutionException e) {
            System.out.print("ERROR: " + e.getMessage() + "\n");
        }
    }

}
