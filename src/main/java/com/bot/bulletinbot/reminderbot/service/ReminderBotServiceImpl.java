package com.bot.bulletinbot.reminderbot.service;

import com.bot.bulletinbot.remindercrud.models.ReminderMessage;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
@Timed
@Service
public class ReminderBotServiceImpl implements ReminderBotService{
    private static final String CREATE_API = "https://bulletin-reminder.herokuapp.com/reminder";
    private static final String GET_BY_USER_API = "https://bulletin-reminder.herokuapp.com/reminder/user/{userid}";
    private static final String GET_BY_ID_API = "https://bulletin-reminder.herokuapp.com/reminder/id/{id}/{userid}";
    private static final String UPDATE_API = "https://bulletin-reminder.herokuapp.com/reminder/{id}";
    private static final String DELETE_API = "https://bulletin-reminder.herokuapp.com/reminder/{id}/{userid}";

    RestTemplate restTemplate = new RestTemplate();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");

    @Override
    @Timed(value = "bot-createReminder", longTask = true)
    public String createReminder(long id, String userid, Date date, String subject) {
        ReminderMessage reminderMessage = new ReminderMessage(id, userid, subject, date, "once");
        ResponseEntity<ReminderMessage> reminder = restTemplate.postForEntity(CREATE_API, reminderMessage, ReminderMessage.class);

        try {
            String message = reminder.getBody().getMessage();
            String strDate = dateFormat.format(reminder.getBody().getRemindTime());

            String reminderid = "" + reminder.getBody().getRemindId();
            return "reminder id:" + reminderid + " dibuat untuk tanggal " + strDate + " dengan subjek " + message;
        }catch (NullPointerException e){
            return "reminder gagal dibuat";
        }
    }

    @Override
    @Timed("bot-getReminderByUser")
    public String getReminderByUser(String userid) {
        ReminderMessage[] reminderMessages = restTemplate.getForObject(GET_BY_USER_API, ReminderMessage[].class, userid);


        String allReminder="";
        for(int i=1; i<reminderMessages.length+1; i++){
            ReminderMessage reminder = reminderMessages[i-1];
            String strDate = dateFormat.format(reminder.getRemindTime());
            allReminder+=""+i+". "+ "(id: " + reminder.getRemindId() + ") untuk: "+strDate+"\nsubjek: " + reminder.getMessage()+"\n\n";
        }
        return allReminder;
    }

    @Override
    @Timed(value = "bot-getReminderByID", longTask = true)
    public String getReminderByID(long id, String userid) {
        Map<String, String> param = new HashMap<>();
        param.put("id",""+id);
        param.put("userid",userid);

        ReminderMessage reminderMessage = restTemplate.getForObject(GET_BY_ID_API, ReminderMessage.class, param);


        String message = (reminderMessage.getMessage()==null)?"":reminderMessage.getMessage();

        String strDate = dateFormat.format(reminderMessage.getRemindTime());

        String reminderid = ""+reminderMessage.getRemindId();
        return "reminder id: "+reminderid+" \nuntuk: "+strDate+" \nsubjek: "+ message;
    }

    @Override
    @Timed("bot-updateReminder")
    public String updateReminder(long id, String userid, Date date, String subject) {
        Map<String, String> param = new HashMap<>();
        param.put("id",""+id);

        ReminderMessage updateReminderMessage = new ReminderMessage(id, userid, subject, date, "once");
//        ResponseEntity<ReminderMessage> reminder = restTemplate.postForEntity(UPDATE_API, updateReminderMessage, ReminderMessage.class, param);
        restTemplate.put(UPDATE_API, updateReminderMessage,param);
        ReminderMessage reminder = restTemplate.getForObject(GET_BY_ID_API, ReminderMessage.class, id,userid);
        try {
            String message = reminder.getMessage();
            String strDate = dateFormat.format(reminder.getRemindTime());
            String reminderid = "" + reminder.getRemindId();
            return "reminder id:" + reminderid + " diubah untuk tanggal " + strDate + " dengan subjek " + message;
        }catch (NullPointerException e) {
            return "reminder gagal diubah";
        }
    }

    @Override
    @Timed("bot-deleteReminder")
    public String deleteReminderById(long id, String userid) {
        restTemplate.delete(DELETE_API,id,userid);
        return "deleted";
    }
}
