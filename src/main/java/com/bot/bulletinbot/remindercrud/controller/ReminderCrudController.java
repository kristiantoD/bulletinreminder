package com.bot.bulletinbot.remindercrud.controller;

import com.bot.bulletinbot.remindercrud.models.ReminderMessage;
import com.bot.bulletinbot.remindercrud.service.ReminderMessageService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path= "/reminder")
public class ReminderCrudController {
    @Autowired
    ReminderMessageService reminderService;

    //create reminder
    @Timed("api-make-reminder")
    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postReminder(@RequestBody ReminderMessage reminderMessage) {
        return ResponseEntity.ok(reminderService.createReminderMessage(reminderMessage));
    }

    //mengambil semua reminder
    @Timed("api-get-all-reminder")
    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<ReminderMessage>> getListReminder() {
        return ResponseEntity.ok(reminderService.getReminderMessage());
    }

    //get reminder untuk user tertentu
    @Timed("api-get-reminder-byuser")
    @GetMapping(path ="/user/{userid}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<ReminderMessage>> getListReminderByUser(@PathVariable(value = "userid") String userid) {
        Iterable<ReminderMessage> reminderMessages = reminderService.getReminderMessageByUser(userid);
        if(reminderMessages == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(reminderMessages);
    }

    //get reminder dengan id tertentu
    @Timed("api-get-reminder-byid")
    @GetMapping(path="/id/{id}/{userid}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<ReminderMessage> getListReminderById(@PathVariable(value = "id") long id, @PathVariable(value = "userid") String userid) {
        ReminderMessage reminderMessage = reminderService.getReminderMessageByID(id, userid);
        //cek apakah reminder ada dan apakah user yang membuat permintaan merupakan pemilik reminder
        if(reminderMessage == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(reminderMessage);
    }

    //mengupdate reminder dengan id tertentu
    @Timed("api-update-reminder-byid")
    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<ReminderMessage> updateReminder(@PathVariable(value = "id") long id, @RequestBody ReminderMessage reminderMessage) {
        String userid = reminderMessage.getUserid();
        ReminderMessage temp = reminderService.getReminderMessageByID(id, userid);
        //cek apakah reminder ada dan apakah user yang membuat permintaan merupakan pemilik reminder
        if(temp == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(reminderService.updateReminder(id, reminderMessage));
    }

    //menghapus reminder dengan id tertentu
    @Timed("api-delete-reminder-byid")
    @DeleteMapping(path = "/{id}/{userid}", produces = {"application/json"})
    public ResponseEntity<ReminderMessage> deleteLog(@PathVariable(value = "id") long id, @PathVariable(value = "userid") String userid) {
        ReminderMessage temp = reminderService.getReminderMessageByID(id, userid);
        //cek apakah reminder ada dan apakah user yang membuat permintaan merupakan pemilik reminder
        if(temp == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        reminderService.deleteReminderById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
