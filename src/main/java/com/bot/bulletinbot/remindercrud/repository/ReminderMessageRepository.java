package com.bot.bulletinbot.remindercrud.repository;

import com.bot.bulletinbot.remindercrud.models.ReminderMessage;
import com.bot.bulletinbot.remindercrud.models.ReminderMessageID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReminderMessageRepository extends JpaRepository<ReminderMessage, ReminderMessageID> {
     @Query(value = "Select * FROM reminder_message WHERE userid=?1", nativeQuery = true)
     List<ReminderMessage> findByUser(String id);

     @Query(value = "Select * From reminder_message WHERE remind_id=?1 and userid=?2", nativeQuery = true)
     ReminderMessage findByRemindId(Long id, String userid);

     @Query(value = "Delete From reminder_message Where remind_id=?1", nativeQuery = true)
     void deleteByRemindId(Long id);

     @Query(value = "Select * From reminder_message Order by remind_time ASC Limit 1", nativeQuery = true)
     ReminderMessage getClosest();
}