package com.bot.bulletinbot.remindercrud.service;


import com.bot.bulletinbot.remindercrud.models.ReminderMessage;

public interface ReminderMessageService {
    ReminderMessage createReminderMessage(ReminderMessage reminderMessage);
    Iterable<ReminderMessage> getReminderMessageByUser(String id);
    Iterable<ReminderMessage> getReminderMessage();
    ReminderMessage getReminderMessageByID(long id, String userid);
    ReminderMessage updateReminder(long id, ReminderMessage reminderMessage);
    void deleteReminderById(long id);
}
