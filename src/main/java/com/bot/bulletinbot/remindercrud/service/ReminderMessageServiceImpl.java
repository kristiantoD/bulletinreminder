package com.bot.bulletinbot.remindercrud.service;

import com.bot.bulletinbot.remindercrud.models.ReminderMessage;
import com.bot.bulletinbot.remindercrud.repository.ReminderMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReminderMessageServiceImpl implements ReminderMessageService{
    @Autowired
    private ReminderMessageRepository repo;

    @Override
    public ReminderMessage createReminderMessage(ReminderMessage reminderMessage) {
        repo.save(reminderMessage);
        return reminderMessage;
    }

    @Override
    public Iterable<ReminderMessage> getReminderMessageByUser(String id) {
        return repo.findByUser(id);
    }

    @Override
    public ReminderMessage getReminderMessageByID(long id, String userid) {
        return repo.findByRemindId(id, userid);
    }

    @Override
    public ReminderMessage updateReminder(long id, ReminderMessage reminderMessage) {
        reminderMessage.setRemindId(id);
        repo.save(reminderMessage);
        return reminderMessage;
    }

    @Override
    public Iterable<ReminderMessage> getReminderMessage() {
        return repo.findAll();
    }

    @Override
    public void deleteReminderById(long id) {
        repo.deleteByRemindId(id);
    }

}
