package com.bot.bulletinbot.reminderbot.controller;

import com.bot.bulletinbot.reminderbot.service.ReminderBotService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;

@ExtendWith(MockitoExtension.class)
public class BotControllerTest {
    @Mock
    LineMessagingClient lineMessagingClient;

    @Mock
    ReminderBotService reminderBotService;

    @Test
    void handleTextEventTestHelp() throws Exception{
        MessageEvent<TextMessageContent> request = new MessageEvent<TextMessageContent>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/showmenu"),
                Instant.now()
        );

    }

}
