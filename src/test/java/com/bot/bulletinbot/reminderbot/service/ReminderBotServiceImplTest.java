package com.bot.bulletinbot.reminderbot.service;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(MockitoExtension.class)
public class ReminderBotServiceImplTest {

    @InjectMocks
    ReminderBotServiceImpl reminderBotService;
    @Test
    void testServiceCreateReminderMessage(){
        Date now = new Date();
        String ans = reminderBotService.createReminder(1L,"user1",now,"ini reminder");
        assertNotEquals(ans,null);
        assertTrue(ans.contains("ini reminder"));
    }

    @Test
    void testServiceGetReminderByID(){
        Date now = new Date();
        reminderBotService.createReminder(2L,"user1",now,"ini reminder2");
        String ans = reminderBotService.getReminderByID(2L,"user1");
        assertNotEquals(ans,null);
        assertTrue(ans.contains("reminder2"));
    }

    @Test
    void testServiceGetReminderByUser(){
        Date now = new Date();
        reminderBotService.createReminder(4L,"user1",now,"ini reminder4");
        String ans = reminderBotService.getReminderByUser("user1");
        assertNotEquals(ans,null);
        assertTrue(ans.contains("reminder4"));
    }

    @Test
    void testServiceUpdateReminder(){
        Date now = new Date();
        reminderBotService.createReminder(3L,"user1",now,"ini reminder3");
        String ans = reminderBotService.updateReminder(3L,"user1",now,"updated reminder");
        assertNotEquals(ans,null);
        assertTrue(ans.contains("updated"));
    }

//    @Test
//    void testServiceDeleteReminder() throws InterruptedException {
//        Date now = new Date();
//        reminderBotService.createReminder(500L,"user111",now,"ini reminder5");
//        sleep(5);
//        String ans = reminderBotService.deleteReminderById(500L,"user111");
//    }
}
