package com.bot.bulletinbot.remindercrud.controller;

import com.bot.bulletinbot.remindercrud.models.ReminderMessage;
import com.bot.bulletinbot.remindercrud.models.ReminderMessageID;
import com.bot.bulletinbot.remindercrud.service.ReminderMessageServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ReminderCrudController.class)
class ReminderCrudControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReminderMessageServiceImpl reminderService;

    private ReminderMessage reminderMessage;

    @BeforeEach
    public void setUp(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021,5,2,23,55);
        Date date=calendar.getTime();
        reminderMessage = new ReminderMessage(1, "user1", "deadline uwu", date,"once");

    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerPostReminder() throws Exception{

        when(reminderService.createReminderMessage(any())).thenReturn(reminderMessage);

        mvc.perform(post("/reminder")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(reminderMessage)))
                .andExpect(jsonPath("$.userid").value("user1"));
    }

    @Test
    void testControllerGetAllReminder() throws Exception{

        Iterable<ReminderMessage> reminderMessages = Arrays.asList(reminderMessage);
        when(reminderService.getReminderMessage()).thenReturn(reminderMessages);

        mvc.perform(get("/reminder").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].userid").value("user1"));
    }

    @Test
    void testControllerGetReminderByUser() throws Exception{

        Iterable<ReminderMessage> reminderMessages = Arrays.asList(reminderMessage);
        when(reminderService.getReminderMessageByUser(anyString())).thenReturn(reminderMessages);

        mvc.perform(get("/reminder/user/user1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].userid").value("user1"));
    }

    @Test
    void testControllerGetReminderByUserNotExists() throws Exception{
        when(reminderService.getReminderMessageByUser(anyString())).thenReturn(null);
        mvc.perform(get("/reminder/user/user2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerGetReminderById() throws Exception{

        when(reminderService.getReminderMessageByID(1,"user1")).thenReturn(reminderMessage);
        mvc.perform(get("/reminder/id/1/user1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.userid").value("user1"));
    }

    @Test
    void testControllerGetNonExistReminder() throws Exception{
        mvc.perform(get("/reminder/id/1/userx").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerUpdateReminder() throws Exception{
        reminderService.createReminderMessage(reminderMessage);
        when(reminderService.getReminderMessageByID(1,"user1")).thenReturn(reminderMessage);
        //Update Reminder message
        reminderMessage.setMessage("UwU");
        when(reminderService.updateReminder(anyInt(), any())).thenReturn(reminderMessage);

        mvc.perform(put("/reminder/1").contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(reminderMessage)))
                .andExpect(status().isOk());

    }

    @Test
    void testControllerUpdateReminderNotExists() throws Exception{
        reminderService.createReminderMessage(reminderMessage);
        when(reminderService.getReminderMessageByID(1,"user1")).thenReturn(null);
        mvc.perform(put("/reminder/1").contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(reminderMessage)))
                .andExpect(status().isNotFound());

    }

    @Test
    void testControllerDeleteReminder() throws Exception{
        when(reminderService.getReminderMessageByID(1,"user1")).thenReturn(reminderMessage);
        reminderService.createReminderMessage(reminderMessage);
        mvc.perform(delete("/reminder/1/user1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void testControllerDeleteOthersReminder() throws Exception{
        when(reminderService.getReminderMessageByID(1,"user1")).thenReturn(reminderMessage);
        reminderService.createReminderMessage(reminderMessage);
        mvc.perform(delete("/reminder/1/user1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void testControllerDeleteNotExists() throws Exception{
        when(reminderService.getReminderMessageByID(1,"user1")).thenReturn(null);
        mvc.perform(delete("/reminder/1/user1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
