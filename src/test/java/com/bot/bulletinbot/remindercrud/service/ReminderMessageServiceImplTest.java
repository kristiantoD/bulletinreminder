package com.bot.bulletinbot.remindercrud.service;


import com.bot.bulletinbot.remindercrud.models.ReminderMessage;
import com.bot.bulletinbot.remindercrud.models.ReminderMessageID;
import com.bot.bulletinbot.remindercrud.repository.ReminderMessageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class ReminderMessageServiceImplTest {
    @Mock
    ReminderMessageRepository repo;

    @InjectMocks
    ReminderMessageServiceImpl messageService;

    private ReminderMessage reminderMessage;
    private ReminderMessageID reminderMessageID;
    @BeforeEach
    public void setUp(){
        reminderMessageID = new ReminderMessageID();
        reminderMessageID.setRemindId(1L);
        reminderMessageID.setUserid("user1");

        reminderMessage = new ReminderMessage();
        reminderMessage.setRemindId(reminderMessageID.getRemindId());
        reminderMessage.setUserid(reminderMessageID.getUserid());
        reminderMessage.setMessage("deadline adpro");
        reminderMessage.setState("once");
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021,3,23,16,43);
        reminderMessage.setRemindTime(calendar.getTime());

        int remindHash = reminderMessageID.hashCode();
    }

    @Test
    void testServiceCreateReminderMessage(){
        lenient().when(messageService.createReminderMessage(reminderMessage)).thenReturn(reminderMessage);
        assertEquals(reminderMessage.getRemindId(),1);
    }

    @Test
    void testServiceGetListAllReminderMessage(){
        Iterable<ReminderMessage> reminders = repo.findAll();
        lenient().when(messageService.getReminderMessage()).thenReturn(reminders);
        Iterable<ReminderMessage> reminderMessages = messageService.getReminderMessage();
        Assertions.assertIterableEquals(reminders, reminderMessages);
    }

    @Test
    void testServiceGetListReminderMessageByUser(){
        Iterable<ReminderMessage> reminders = repo.findByUser("user1");
        lenient().when(messageService.getReminderMessageByUser("user1")).thenReturn(reminders);
        Iterable<ReminderMessage> reminderMessages = messageService.getReminderMessageByUser("user1");
        Assertions.assertIterableEquals(reminders, reminderMessages);
    }

    @Test
    void testServiceGetListReminderMessageById(){
        lenient().when(messageService.getReminderMessageByID(1,"user1")).thenReturn(reminderMessage);
        ReminderMessage result = messageService.getReminderMessageByID(1, "user1");
        assertEquals(result.getRemindId(), reminderMessage.getRemindId());
    }

    @Test
    void testServiceDeleteReminderMessage(){
        messageService.createReminderMessage(reminderMessage);
        messageService.deleteReminderById(1);
        lenient().when(messageService.getReminderMessageByID(1,"user1")).thenReturn(null);
        assertEquals(null, messageService.getReminderMessageByID(reminderMessage.getRemindId(),reminderMessage.getUserid()));
    }

    @Test
    void testServiceUpdateReminderMessage(){
        messageService.createReminderMessage(reminderMessage);
        String currentMessage = reminderMessage.getMessage();
        //change message
        reminderMessage.setMessage("aaaaaaaaaaaa");

        lenient().when(messageService.updateReminder(reminderMessage.getRemindId(),reminderMessage)).thenReturn(reminderMessage);
        ReminderMessage result = messageService.updateReminder(reminderMessage.getRemindId(),reminderMessage);

        assertNotEquals(result.getMessage(), currentMessage);
        assertEquals(result.getState(), reminderMessage.getState());


    }

}
